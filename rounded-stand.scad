// clang-format off
include <utils/utils.scad>;
// clang-format on

/* [Stand] */
stand_diameter = 50;    // [10:0.1:100]
stand_edge_radius = 10; // [0:0.1:100]
stand_edges = 0;        // [0:1:10]
stand_height = 30;      // [0:0.1:100]

/* [Hole] */
hole_edges = 0;        // [0:1:10]
hole_edge_radius = 10; // [0:0.1:50]
hole_diameter = 30;    // [5:0.1:100]
hole_depth = 15;       // [1:0.1:100]
hole_rotation = 0;     // [-180:1:180]

/* [Precision] */
$fs = $preview ? 3 : 1;
$fa = $preview ? 10 : 2;
epsilon = $fs / 100;

module
regular_polygon(d, edges = 0)
{
  circle(d = d, $fn = edges > 2 ? edges : max($fn, PI * d / $fs, 360 / $fa));
}

module
block(d, h, edges = 0, radius)
{
  difference()
  {
    minkowski()
    {
      linear_extrude(h - radius) offset(-radius)
        regular_polygon(d = d, edges = edges);
      sphere(radius);
    }
    translate([ 0, 0, -(radius + epsilon) ]) linear_extrude(radius + epsilon)
      offset(radius + epsilon) regular_polygon(d = d, edges = edges);
  }
}

difference()
{
  block(d = stand_diameter,
        h = stand_height,
        edges = stand_edges,
        radius = stand_edge_radius);
  translate([ 0, 0, stand_height + epsilon ]) rotate([ 180, 0, hole_rotation ])
    block(d = hole_diameter,
          h = hole_depth,
          edges = hole_edges,
          radius = hole_edge_radius);
}
